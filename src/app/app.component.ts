import { Component, OnInit } from '@angular/core';

import { Organization } from './data-model/organization';
import { IWorkflow } from './data-model/workflow/workflow';
import { WorkflowService } from './services/workflow.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  workflows: IWorkflow[];
  errorMessage: string;

  currentOrg: Organization = new Organization();
  orgs: Organization[] = [
    new Organization("1", "Org 1", "Org 1 Code"),
    new Organization("2", "Org 2", "Org 1 Code"),
    new Organization("3", "Org 3", "Org 1 Code"),
  ];

  title = 'app';

  constructor(private _workflowService: WorkflowService) {

  }

  ngOnInit(): void {
    this._workflowService.getWorkflows()
      .subscribe(
      workflows => {
        this.workflows = workflows;
        console.log(this.workflows);
      },
      error => this.errorMessage = <any>error
      );
  }
}
