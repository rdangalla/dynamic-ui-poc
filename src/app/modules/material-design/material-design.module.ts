import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule, MaterialModule, MdTabsModule, MdSelectModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import 'hammerjs'

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports:[
    MdButtonModule, 
    MdCardModule, 
    MdMenuModule, 
    MdToolbarModule, 
    MdIconModule, 
    MaterialModule, 
    MdTabsModule, 
    MdSelectModule
  ]
})
export class MaterialDesignModule { }
