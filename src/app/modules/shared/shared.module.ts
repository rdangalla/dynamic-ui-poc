import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ShipperComponent } from './smart-components/shipper/shipper.component';

import { MaterialDesignModule } from '../material-design/material-design.module';

@NgModule({
  imports: [
    CommonModule, FormsModule, MaterialDesignModule
  ],
  declarations: [ShipperComponent],
  exports: [
    ShipperComponent
  ]
})
export class SharedModule { }
