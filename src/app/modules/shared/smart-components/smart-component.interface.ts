export interface SmartComponent {
    isValid(): Boolean;
}