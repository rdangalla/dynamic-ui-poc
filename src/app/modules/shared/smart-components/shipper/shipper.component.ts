import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { SmartComponent } from '../smart-component.interface';
import { Organization } from '../../../../data-model/organization';
import { Profile } from '../../../../data-model/profile';


@Component({
    selector: "shipper",
    templateUrl: "./shipper.component.html",
    styleUrls: ["./shipper.component.scss"]
})
export class ShipperComponent implements SmartComponent, OnInit {

    constructor() { }

    isValid(): Boolean {
        return this.shipper !== null;
    }

    ngOnInit() {
        this._configure();
    }

    @Input("organization") organizationIWork: Organization;
    @Input("user") me: Profile;
    @Input("shipper") shipper: Organization;
    @Input("partners") partnersAndThisOrgList: Array<Organization>
    @Output("shipperChanged") shipperChanged: EventEmitter<Organization> = new EventEmitter<Organization>();
    iWorkForShipper: Boolean;

    onChange() {
        this.shipperChanged.emit(this.shipper);
    }

    _configure() {
        // if (!this.shipper && this.organizationIWork.nature.indexOf("absolute_demand") !== -1 && this.me.organizations.indexOf(this.organizationIWork._id) !== -1) {
        //     this.iWorkForShipper = true;
        //     this.shipper = this.organizationIWork;
        //     setTimeout(() => {
        //         this.shipperChanged.emit(this.shipper);
        //     }, 100);
        // } else {
        //     this.iWorkForShipper = false;
        // }
    }

}