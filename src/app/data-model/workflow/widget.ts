import { IOutput } from "./output";
import { IInput } from "./input";

export interface IWidget {
    name: string,
    smartComponent: string,
    inputs: IInput[],
    outputs: IOutput[]
}