import { IStep } from "./step";

export interface IWorkflow {
    workflow: string,
    selections: string[],
    steps: IStep[]
}