export interface IStepDependant {
    parentSmartComponent: string,
    childSmartComponent: boolean,
    varMap: any[]
}