import { IStepDependant } from "./stepDependant";
import { IWidget } from "./widget";

export interface IStep {
    step: string,
    viewed: boolean,
    stepDependant: IStepDependant[],
    widgets: IWidget[]
}