﻿export class Profile {
    constructor(
        public _id?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public primaryMobileNumber?: string,
        public contactNumbers?: Array<string>,
        public nic?: string,
        public passport?: string,
        public dateOfBirth?: string,
        public language?: string,
        public activeStatus?: string,
        public curruncy?: string,
        public curruncyFormate?: string,
        public organizations?: Array<string>,
        public profilePicture?: string,
        public resourcesIdArray?: Array<string>,
        public organizationsHistory?: Array<string>
    ) { }
}