﻿import { User } from "./user";

export class Organization {
    constructor(
        public _id?: string,
        public registeredName?: string,
        public tickerCode?: string,
        public type?: string,
        public registeredNumber?: string,
        public registeredAddress?: string,
        public operatingAddress?: string,
        public nature?: Array<string>,
        public documents?: Array<OrganizationDocument>,
        public locations?: Array<string>,
        public import_countries?: Array<string>,
        public export_countries?: Array<string>,
        public suppliers?: Array<string>,
        public buyers?: Array<string>,
        public su?: string,
        public url?: string,
        public createdTime?: number,
        public contactEmails?: Array<string>,
        public contactNumbers?: Array<string>,
        public faxes?: Array<string>,
        public vatNo?: string,
        public tinNo?: string,
        public principleContactPerson?: User
    ) { }
}

export class OrganizationDocument {
    constructor();
    constructor(
        public file_id?: string,
        public name?: string
    ) { }
}