﻿export class User {
    constructor(
        public id?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public phoneNumber?: string,
        public userName?: string,
        public password?: string
    ) { }
}