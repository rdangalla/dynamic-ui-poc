import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { IWorkflow } from '../data-model/workflow/workflow';

@Injectable()
export class WorkflowService {

    private _productUrl = './assets/data/mockData.json'; //'http://ec2-13-126-31-178.ap-south-1.compute.amazonaws.com:8765/api/workflow/sampleflow';
    
    constructor(private _http: HttpClient) { }

    getWorkflows(): Observable<IWorkflow[]> {
        return this._http.get<IWorkflow[]>(this._productUrl)
            //.do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    handleError(err: HttpErrorResponse) {
        console.log(err.message);
        return Observable.throw(err.message);
    }
}